## Lab 8 (Arduino lab)

### Joystick, Arduino and a computer fan

**Procedure**

1. Design a circuit, with which you can control the rotating speed of a computer fan with a joystick.
When joystick is in the midsection (rest position) the fan is stopped. When joystick is turned
towards other limit (no matter up or down), the fan starts to rotate. The more joystick is turned, the
faster the fan will rotate.

2. Add a 7-segment display to your design so, that when the joystick is in the midsection there will
be number 0 on the display. When the joystick is turned to the limit position, there should be
number 9 on the display.

3. Take off the computer fan and replace it with a RGB-led. When the joystick is in the midsection,
the color of the RGB-led should be blue. When the joystick is turned to some direction, the color of
the led should be green and when the joystick is on it’s limit, the color of the led should be red.