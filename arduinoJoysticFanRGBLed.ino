/*
Authors: Jimi Toiviainen, Matti Lindholm
A circuit, with which you can control the rotating speed of a computer fan and color of RGB-led with a joystick seamlessly.
<<<<<<< HEAD
=======

TODO:
Add 7-seg display
>>>>>>> 55c57d62de349d313e86658a8622395dc91ec985
*/

#define FLOAT_TO_INT(x) ((x)>=0?(int)((x)+0.5):(int)((x)-0.5)) // round float point number to integer

int analogPin = A3; // potentiometer wiper (middle terminal) connected to analog pin 3 outside leads to ground and +5V
int val = 0; // variable to store the value read
int maxval = 1023;
int joystickPosition = 0;

int redVal = 0;
int greenVal = 0;
int blueVal = 0;

int redPin = 10;
int greenPin = 6;
int bluePin = 9;
int fanPin = 5;

void setup()
{
  Serial.begin(9600); // setup serial
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  pinMode(fanPin, OUTPUT);
}

void loop()
{
  val = analogRead(analogPin); // read the input pin
  if(val > maxval){ // Calibrate if we get higher than default
    maxval = val;
  }
  
  joystickPosition = val - maxval / 2;
  float digitFloat = (((float)joystickPosition/512));
  float centered = abs(digitFloat);
  analogWrite(5, centered*255);
  
  // Digit for the 7-seg display
  int digit = FLOAT_TO_INT( digitFloat * 9 );
  
  if(abs(digitFloat)>0.5){
    
    greenVal = 0;
    redVal = (centered-0.5)*255*2;
    blueVal = abs(centered-1)*255*2;
    
  } else if(abs(digitFloat)<0.5){
    
    greenVal = abs(centered-0.5)*255*2;
    redVal = 0;
   	blueVal = abs(centered*255*2);
  
  analogWrite(redPin, redVal);
  analogWrite(greenPin, greenVal);
  analogWrite(bluePin, blueVal);
  analogWrite(fanPin, centered*255);
}